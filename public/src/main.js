(function () {
    let h1 = document.getElementById("maincanvas");
    /**
     * 
     * @param {Date} date 
     */
    function setTimeToZero(date) {
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
    }

    const timeToday = new Date();
    const timeTommorow = new Date();

    setTimeToZero(timeToday);
    setTimeToZero(timeTommorow);
    timeTommorow.setDate( timeToday.getDate() + 1);

    /**
     * 
     * @param {Date} timeToday 
     * @param {Date} timeTommorow 
     */
    function timeDiff(timeTommorow){
        const timeNow = new Date();
        return Math.trunc((timeTommorow.getTime() - timeNow.getTime())/ 1000);
    }
    setInterval(()=>{
        let num = ((timeDiff(timeTommorow)* 100)/86400).toFixed(2) + "%";
        h1.innerText = num;
    }, 800);
})()